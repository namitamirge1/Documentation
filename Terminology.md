#### [Return to main Index page](https://github.com/hydropero/Terminology)

# Terminology

- # [Web](https://github.com/hydropero/Terminology/blob/main/Web.md)
- ### Web Services
  - Domain: 
    <br> For example: store.yourwebsite.com
    <br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;subdomain ^ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ^ Domain  &nbsp;&nbsp;&nbsp; ^ TLD (Top Level Domain)

- # Cloud
- VM - Virtual machine


- ## AWS
- ### Amazon EC2
  - Amazon EC2 = Elastic Compute Cloud
  - Amazon EBS - Elastic Block Store
  - Amazon EFS - Elastic File System
  - Amazon VPC = Virtual Private Cloud
  - RI- Reserved Instance
  - AURI - All Upfront Reserved Instance
  - PURI - Partial Upfront Reserved Instance
  - NURI - No Upfront Reserved Instance
  - Amazon IAM- Amazon Identity & Access Management
  - Amazon KMS- Amazon Key Management Service
  - Amazon ECS- Amazon Elastic Container Service
  - Amazon ECR- Amazon Elastic Container Registry
  - Amazon EKS- Amazon Elastic Kubernetes Service
  - SSH- Secure Shell or Secure Socket Shell
  - SSL - Secure Socket Layer 

- ### Basic Computing 
- OS - Operating system
- GB- Gigabyte (1 GB = 10^9 bytes or 1 billion bytes)
- GiB- Gibibyte (1 GiB= 2^30 bytes or 1,073,741,824 bytes)
- .pem- Privacy Enhanced Mail
- .ppk- Putty Private Key

- ### Amazon S3 - Amazon simple storage service
- AMI - Amazon Machine Image

- #### 3 Cloud Service models : 
  - IaaS (Infrastructure as a service)
  - PaaS (Platform as a service)
  - SaaS (Software as a service)
Shebang - #!
